# Toko Novel dan Buku
<div align="center"><img src="hasil SS/logo.jpg"  width="280" height="280">

**UTS Mobile Programming 2**</div>

## Tentang Aplikasi 
Aplikasi "TOKO BUKU dan NOVEL" adalah sebuah aplikasi penjualan yang berbasis mobile didalam aplikasi ini terdapat berbagai jenis buku dan novel yang tersedia. didalam aplikasi tersebut ada page home, page detail, jenis kategori, dan page about yang didalamnya terdapat biodata anggota kelompok.

## Anggota Kelompok
* [Ahmad Hermansyah](https://gitlab.com/ahmadhermansyah3) 18111181
* [Asep Saepul Ulum Bahri](https://gitlab.com/saepula995) 13111055
* [Azka Ryvaldo Nurramadan](https://gitlab.com/astaryval) 18111187
* [Hafidt Amanulloh](https://gitlab.com/hafidt) 18111388
* [Reza Dwi Putra](https://gitlab.com/rezadp) 18111389

## Job Description Anggota
* **Ahmad Hermansyah** (sebagai ketua kelompok dan menentukan alur aplikasi tersebut)
* **Asep Saepul Ulum Bahri** (yang menentukan judul aplikasi)
* **Azka Ryvaldo Nurramadan** (yang menentukan alur aplikasi)
* **Hafidt Amanulloh Multyazi** (yang mencari referensi template aplikasi)
* **Reza Dwi Putra** (yang menentukan desain dalam aplikasi)


## Hasil Screenshot Aplikasi

<img src="hasil SS/home.jpeg"  width="300" height="520">
<img src="hasil SS/romantik.jpeg"  width="300" height="520">
<img src="hasil SS/adventure.jpeg"  width="300" height="520">
<img src="hasil SS/religi.jpeg"  width="300" height="520">
<img src="hasil SS/horror.jpeg"  width="300" height="520">
<img src="hasil SS/detail.jpeg"  width="300" height="520">
<img src="hasil SS/biodata1.jpeg"  width="300" height="520">
<img src="hasil SS/biodata2.jpeg"  width="300" height="520">

## Terima Kasih

[Flutter logo]: https://raw.githubusercontent.com/flutter/website/master/src/_assets/image/flutter-lockup.png
[flutter.dev]: https://flutter.dev