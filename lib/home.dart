import 'data.dart';
import 'package:flutter/material.dart';
import 'package:uts/about.dart';
import 'package:uts/kategori.dart';
import 'main.dart';

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    //app bar
    final appBar = AppBar(
      elevation: .5,
      // leading: IconButton(
      //   icon: Icon(Icons.menu),
      //   onPressed: () {},
      // ),
      title: Text('Toko Buku dan Novel'),
      actions: <Widget>[
        IconButton(
          icon: Icon(Icons.search),
          onPressed: () {},
        )
      ],
    );

    ///create book tile hero
    createTile(Book book) => Hero(
          tag: book.title,
          child: Material(
            elevation: 15.0,
            shadowColor: Colors.green.shade900,
            child: InkWell(
              onTap: () {
                Navigator.pushNamed(context, 'detail/${book.title}');
              },
              child: Image(
                image: AssetImage(book.image),
                fit: BoxFit.cover,
              ),
            ),
          ),
        );

    ///create book grid tiles
    final grid = CustomScrollView(
      primary: false,
      slivers: <Widget>[
        SliverPadding(
          padding: EdgeInsets.all(16.0),
          sliver: SliverGrid.count(
            childAspectRatio: 2 / 3,
            crossAxisCount: 3,
            mainAxisSpacing: 20.0,
            crossAxisSpacing: 20.0,
            children: books.map((book) => createTile(book)).toList(),
          ),
        )
      ],
    );

    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      appBar: appBar,
      drawer: new Drawer(
        child: new ListView(
          children: <Widget> [
            new DrawerHeader(child: new Text('Menu'),),
          new ListTile(
              title: new Text('Home'),
              onTap: () {
                Navigator.push(context, MaterialPageRoute(builder: (context) => MyApp()));
              },
            ),
            new ListTile(
              title: new Text('Kategori Romantik'),
              onTap: () {
                Navigator.push(context, MaterialPageRoute(builder: (context) => KategoriApp(ktgr: 'Kategori Romantik')));
              },
            ),
            new ListTile(
              title: new Text('Kategori Sejarah'),
              onTap: () {
                Navigator.push(context, MaterialPageRoute(builder: (context) => KategoriApp(ktgr: 'Kategori Sejarah')));
              },
            ),
            new ListTile(
              title: new Text('Kategori Horror'),
              onTap: () {
                Navigator.push(context, MaterialPageRoute(builder: (context) => KategoriApp(ktgr: 'Kategori Horror')));
              },
            ),
            new ListTile(
              title: new Text('Kategori Adventure'),
              onTap: () {
                Navigator.push(context, MaterialPageRoute(builder: (context) => KategoriApp(ktgr: 'Kategori Adventure')));
              },
            ),
            new ListTile(
              title: new Text('Kategori Religi'),
              onTap: () {
                Navigator.push(context, MaterialPageRoute(builder: (context) => KategoriApp(ktgr: 'Kategori Religi')));
              },
            ),
            new Divider(),
            new ListTile(
              title: new Text('About'),
              onTap: () {
                Navigator.push(context, MaterialPageRoute(builder: (context) => About()));
              },
            ),
            new Divider(),
            new ListTile(
              title: new Text('Logout'),
              onTap: () {},
            ),
          ],
        )
      ),
      body: grid,
    );
  }
}